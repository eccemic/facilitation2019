# facilitation2019

```
cd existing_repo
git remote add origin https://gitlab.com/eccemic/facilitation2019.git
git branch -M main
git push -uf origin main
```

## Description
Code to simulate model, generate results and make plots for the paper on _facilitation_ by Piccardi, Vessman, Mitri (2019) PNAS https://doi.org/10.1073/pnas.1906172116

## Usage
The directory 'plots' contain the code to generate the figures for the paper
and have some basic examples on how to use the model. More examples will follow.

## Support
Send an email to bjorn.vessman@unil.ch

## Authors and acknowledgment
Björn Vessman, University of Lausanne

## License
For open source projects, say how it is licensed.
